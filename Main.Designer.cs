﻿namespace GrapecityReportsLibrary
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.flowPnlMain = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.ar_linkbutton = new System.Windows.Forms.ToolTip(this.components);
            this.homePageControl1 = new GrapecityReportsLibrary.Controls.HomePageControl();
            this.reportViewerControl1 = new GrapecityReportsLibrary.Controls.ReportViewerControl();
            this.industryControl1 = new GrapecityReportsLibrary.Controls.IndustryControl();
            this.functionControl1 = new GrapecityReportsLibrary.Controls.FunctionControl();
            this.reportBrowserViewerControl1 = new GrapecityReportsLibrary.Controls.ReportBrowserViewerControl();
            this.reportsMenu1 = new GrapecityReportsLibrary.Controls.ReportsMenuControl();
            this.productLogoControl1 = new GrapecityReportsLibrary.Controls.ProductLogoControl();
            this.linkButtonControl2 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.linkButtonControl1 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.linkButtonControl3 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.linkButtonControl4 = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.panel1.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlMain.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(52)))), ((int)(((byte)(75)))));
            this.panel1.Controls.Add(this.productLogoControl1);
            this.panel1.Controls.Add(this.panel8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1264, 58);
            this.panel1.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.linkButtonControl2);
            this.panel8.Controls.Add(this.linkButtonControl1);
            this.panel8.Controls.Add(this.linkButtonControl3);
            this.panel8.Controls.Add(this.linkButtonControl4);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(731, 0);
            this.panel8.Margin = new System.Windows.Forms.Padding(0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(533, 58);
            this.panel8.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlMain);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 58);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1264, 671);
            this.panel2.TabIndex = 1;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.homePageControl1);
            this.pnlMain.Controls.Add(this.reportViewerControl1);
            this.pnlMain.Controls.Add(this.industryControl1);
            this.pnlMain.Controls.Add(this.functionControl1);
            this.pnlMain.Controls.Add(this.reportBrowserViewerControl1);
            this.pnlMain.Controls.Add(this.flowPnlMain);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(260, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1004, 671);
            this.pnlMain.TabIndex = 2;
            // 
            // flowPnlMain
            // 
            this.flowPnlMain.AutoScroll = true;
            this.flowPnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowPnlMain.Location = new System.Drawing.Point(0, 0);
            this.flowPnlMain.Name = "flowPnlMain";
            this.flowPnlMain.Size = new System.Drawing.Size(1004, 671);
            this.flowPnlMain.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.reportsMenu1);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(260, 671);
            this.panel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.webBrowser1);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(0, 511);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(260, 160);
            this.panel4.TabIndex = 15;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::GrapecityReportsLibrary.Properties.Resources.葡萄城报表;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(260, 160);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "http://www.grapecity.com.cn/enterprise-solutions/activereports_server/activity/?u" +
    "tm_source=Demo&utm_medium=ActiveReportsServer&utm_term=GrapecityReportsLibrary&u" +
    "tm_content=promotion";
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(84, 43);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(133, 94);
            this.webBrowser1.TabIndex = 1;
            // 
            // homePageControl1
            // 
            this.homePageControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.homePageControl1.EventData = null;
            this.homePageControl1.EventName = null;
            this.homePageControl1.Location = new System.Drawing.Point(0, 0);
            this.homePageControl1.Margin = new System.Windows.Forms.Padding(3, 317, 3, 317);
            this.homePageControl1.Name = "homePageControl1";
            this.homePageControl1.Size = new System.Drawing.Size(1004, 671);
            this.homePageControl1.TabIndex = 1;
            this.homePageControl1.ReportClick += new GrapecityReportsLibrary.Controls.HomePageControl.ReportClickEvent(this.homePageControl1_ReportClick);
            // 
            // reportViewerControl1
            // 
            this.reportViewerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewerControl1.EventData = null;
            this.reportViewerControl1.EventName = null;
            this.reportViewerControl1.Location = new System.Drawing.Point(0, 0);
            this.reportViewerControl1.Name = "reportViewerControl1";
            this.reportViewerControl1.Size = new System.Drawing.Size(1004, 671);
            this.reportViewerControl1.TabIndex = 0;
            // 
            // industryControl1
            // 
            this.industryControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.industryControl1.EventData = null;
            this.industryControl1.EventName = null;
            this.industryControl1.Location = new System.Drawing.Point(0, 0);
            this.industryControl1.Name = "industryControl1";
            this.industryControl1.Size = new System.Drawing.Size(1004, 671);
            this.industryControl1.TabIndex = 2;
            // 
            // functionControl1
            // 
            this.functionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.functionControl1.EventData = null;
            this.functionControl1.EventName = null;
            this.functionControl1.Location = new System.Drawing.Point(0, 0);
            this.functionControl1.Name = "functionControl1";
            this.functionControl1.Size = new System.Drawing.Size(1004, 671);
            this.functionControl1.TabIndex = 3;
            // 
            // reportBrowserViewerControl1
            // 
            this.reportBrowserViewerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportBrowserViewerControl1.EventData = null;
            this.reportBrowserViewerControl1.EventName = null;
            this.reportBrowserViewerControl1.Location = new System.Drawing.Point(0, 0);
            this.reportBrowserViewerControl1.Name = "reportBrowserViewerControl1";
            this.reportBrowserViewerControl1.Size = new System.Drawing.Size(1004, 671);
            this.reportBrowserViewerControl1.TabIndex = 99;
            // 
            // reportsMenu1
            // 
            this.reportsMenu1.AutoScroll = true;
            this.reportsMenu1.AutoSize = true;
            this.reportsMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.reportsMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportsMenu1.EventData = null;
            this.reportsMenu1.EventName = null;
            this.reportsMenu1.Location = new System.Drawing.Point(0, 0);
            this.reportsMenu1.Margin = new System.Windows.Forms.Padding(0);
            this.reportsMenu1.Name = "reportsMenu1";
            this.reportsMenu1.ReportMenu = null;
            this.reportsMenu1.SelectedReport = null;
            this.reportsMenu1.Size = new System.Drawing.Size(260, 511);
            this.reportsMenu1.TabIndex = 4;
            this.reportsMenu1.ReportClick += new GrapecityReportsLibrary.Controls.ReportsMenuControl.MenuItemClickEvent(this.reportsMenu1_ReportClick);
            // 
            // productLogoControl1
            // 
            this.productLogoControl1.BackColor = System.Drawing.Color.Transparent;
            this.productLogoControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.productLogoControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.productLogoControl1.EventData = null;
            this.productLogoControl1.EventName = null;
            this.productLogoControl1.Location = new System.Drawing.Point(0, 0);
            this.productLogoControl1.Name = "productLogoControl1";
            this.productLogoControl1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.productLogoControl1.Size = new System.Drawing.Size(330, 58);
            this.productLogoControl1.TabIndex = 5;
            this.productLogoControl1.ProductLogoClick += new GrapecityReportsLibrary.Controls.ProductLogoControl.ProductLogoClickEvent(this.productLogoControl1_ProductLogoClick);
            // 
            // linkButtonControl2
            // 
            this.linkButtonControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkButtonControl2.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkButtonControl2.ButtonText = "ActiveReports 报表控件";
            this.linkButtonControl2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl2.Dock = System.Windows.Forms.DockStyle.Right;
            this.linkButtonControl2.EventData = null;
            this.linkButtonControl2.EventName = null;
            this.linkButtonControl2.EventType = null;
            this.linkButtonControl2.EventValue = null;
            this.linkButtonControl2.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.linkButtonControl2.ForeColor = System.Drawing.Color.White;
            this.linkButtonControl2.LinkUrl = "https://www.grapecity.com.cn/developer/activereports?utm_source=Demo&utm_medium=A" +
    "ctiveReports&utm_term=GrapecityReportsLibrary&utm_content=arwebsite";
            this.linkButtonControl2.Location = new System.Drawing.Point(50, 0);
            this.linkButtonControl2.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl2.Name = "linkButtonControl2";
            this.linkButtonControl2.Size = new System.Drawing.Size(175, 58);
            this.linkButtonControl2.TabIndex = 10;
            this.linkButtonControl2.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl2_ButtonClick);
            // 
            // linkButtonControl1
            // 
            this.linkButtonControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkButtonControl1.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.linkButtonControl1.ButtonText = "报表服务器";
            this.linkButtonControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.linkButtonControl1.EventData = null;
            this.linkButtonControl1.EventName = null;
            this.linkButtonControl1.EventType = null;
            this.linkButtonControl1.EventValue = null;
            this.linkButtonControl1.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.linkButtonControl1.ForeColor = System.Drawing.Color.White;
            this.linkButtonControl1.LinkUrl = "http://www.grapecity.com.cn/enterprise-solutions/activereports_server/?utm_source" +
    "=Demo&utm_medium=ActiveReportsServer&utm_term=GrapecityReportsLibrary&utm_conten" +
    "t=arswebsite";
            this.linkButtonControl1.Location = new System.Drawing.Point(225, 0);
            this.linkButtonControl1.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl1.Name = "linkButtonControl1";
            this.linkButtonControl1.Size = new System.Drawing.Size(128, 58);
            this.linkButtonControl1.TabIndex = 6;
            this.linkButtonControl1.Visible = false;
            this.linkButtonControl1.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl1_ButtonClick);
            // 
            // linkButtonControl3
            // 
            this.linkButtonControl3.ButtonBackColor = System.Drawing.Color.Empty;
            this.linkButtonControl3.ButtonText = "技术资料";
            this.linkButtonControl3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl3.Dock = System.Windows.Forms.DockStyle.Right;
            this.linkButtonControl3.EventData = null;
            this.linkButtonControl3.EventName = null;
            this.linkButtonControl3.EventType = null;
            this.linkButtonControl3.EventValue = null;
            this.linkButtonControl3.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.linkButtonControl3.ForeColor = System.Drawing.Color.White;
            this.linkButtonControl3.LinkUrl = "http://gcdn.gcpowertools.com.cn/showtopic-21777-1-1.html?utm_source=Demo&utm_medi" +
    "um=ActiveReportsServer&utm_term=GrapecityReportsLibrary&utm_content=help";
            this.linkButtonControl3.Location = new System.Drawing.Point(353, 0);
            this.linkButtonControl3.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl3.Name = "linkButtonControl3";
            this.linkButtonControl3.Size = new System.Drawing.Size(90, 58);
            this.linkButtonControl3.TabIndex = 8;
            this.linkButtonControl3.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl1_ButtonClick);
            // 
            // linkButtonControl4
            // 
            this.linkButtonControl4.ButtonBackColor = System.Drawing.Color.Empty;
            this.linkButtonControl4.ButtonText = "关于我们";
            this.linkButtonControl4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkButtonControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.linkButtonControl4.EventData = null;
            this.linkButtonControl4.EventName = null;
            this.linkButtonControl4.EventType = null;
            this.linkButtonControl4.EventValue = null;
            this.linkButtonControl4.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.linkButtonControl4.ForeColor = System.Drawing.Color.White;
            this.linkButtonControl4.LinkUrl = null;
            this.linkButtonControl4.Location = new System.Drawing.Point(443, 0);
            this.linkButtonControl4.Margin = new System.Windows.Forms.Padding(0);
            this.linkButtonControl4.Name = "linkButtonControl4";
            this.linkButtonControl4.Size = new System.Drawing.Size(90, 58);
            this.linkButtonControl4.TabIndex = 9;
            this.linkButtonControl4.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.linkButtonControl4_ButtonClick);
            // 
            // Main
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1264, 729);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "葡萄城报表模板库";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlMain.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel8;
        private Controls.ReportsMenuControl reportsMenu1;
        private Controls.ReportViewerControl reportViewerControl1;
        private Controls.ReportBrowserViewerControl reportBrowserViewerControl1;
        private Controls.ProductLogoControl productLogoControl1;
        private Controls.HomePageControl homePageControl1;
        private Controls.LinkButtonControl linkButtonControl1;
        private Controls.LinkButtonControl linkButtonControl3;
        private Controls.LinkButtonControl linkButtonControl4;
        private Controls.IndustryControl industryControl1;
        private Controls.FunctionControl functionControl1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private Controls.LinkButtonControl linkButtonControl2;
        private System.Windows.Forms.FlowLayoutPanel flowPnlMain;
        private System.Windows.Forms.ToolTip ar_linkbutton;
    }
}

