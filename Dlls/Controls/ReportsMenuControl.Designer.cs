﻿namespace GrapecityReportsLibrary.Controls
{
    partial class ReportsMenuControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFunction = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.btnIndustry = new GrapecityReportsLibrary.Controls.LinkButtonControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(83)))), ((int)(((byte)(120)))));
            this.panel1.Controls.Add(this.btnFunction);
            this.panel1.Controls.Add(this.btnIndustry);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(260, 45);
            this.panel1.TabIndex = 1;
            // 
            // btnFunction
            // 
            this.btnFunction.BackColor = System.Drawing.Color.Transparent;
            this.btnFunction.ButtonBackColor = System.Drawing.Color.Transparent;
            this.btnFunction.ButtonText = "报表功能";
            this.btnFunction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFunction.EventData = null;
            this.btnFunction.EventName = null;
            this.btnFunction.EventType = null;
            this.btnFunction.EventValue = null;
            this.btnFunction.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.btnFunction.ForeColor = System.Drawing.Color.White;
            this.btnFunction.LinkUrl = null;
            this.btnFunction.Location = new System.Drawing.Point(130, 10);
            this.btnFunction.Margin = new System.Windows.Forms.Padding(0);
            this.btnFunction.Name = "btnFunction";
            this.btnFunction.Size = new System.Drawing.Size(120, 35);
            this.btnFunction.TabIndex = 1;
            this.btnFunction.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.buttonControl1_ButtonClick);
            // 
            // btnIndustry
            // 
            this.btnIndustry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.btnIndustry.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.btnIndustry.ButtonText = "行业案例";
            this.btnIndustry.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnIndustry.EventData = null;
            this.btnIndustry.EventName = null;
            this.btnIndustry.EventType = null;
            this.btnIndustry.EventValue = null;
            this.btnIndustry.Font = new System.Drawing.Font("微软雅黑", 10F);
            this.btnIndustry.ForeColor = System.Drawing.Color.White;
            this.btnIndustry.LinkUrl = null;
            this.btnIndustry.Location = new System.Drawing.Point(10, 10);
            this.btnIndustry.Margin = new System.Windows.Forms.Padding(0);
            this.btnIndustry.Name = "btnIndustry";
            this.btnIndustry.Size = new System.Drawing.Size(120, 35);
            this.btnIndustry.TabIndex = 0;
            this.btnIndustry.ButtonClick += new GrapecityReportsLibrary.Controls.LinkButtonControl.ButtonClickEvent(this.buttonControl1_ButtonClick);
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 45);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(260, 30);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.AutoScrollMargin = new System.Drawing.Size(1, 1);
            this.panel3.AutoSize = true;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(260, 234);
            this.panel3.TabIndex = 3;
            // 
            // ReportsMenuControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ReportsMenuControl";
            this.Size = new System.Drawing.Size(260, 309);
            this.Load += new System.EventHandler(this.ReportsMenu_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private LinkButtonControl btnFunction;
        private LinkButtonControl btnIndustry;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
    }
}
