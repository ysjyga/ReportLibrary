﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ReportDescriptionControl : GCRLBaseControl
    {
        public ReportDescriptionControl()
        {
            InitializeComponent();
        }

        private void ReportDescriptionControl_Load(object sender, EventArgs e)
        {
            //webBrowser1.DocumentText = "<html><head><link href='http://demo.gcpowertools.com.cn/ActiveReports/ASPNET/ControlExplorer/css/style.css' rel='stylesheet' type='text/css'></head><body><div class='reportintroduce'>    <ul>        <li><div class='li-header'>报表功能</div><div>该报表将图表和钻取分析相结合，一级报表分别显示了多年销售数据的汇总图表，通过点击操作可以显示每年销售数据明细。</div></li>        <li>            <div class='li-header'>使用说明</div>            <ul>                <li>点击【年度汇总】左侧的“+”号进行“展开”操作，可以查看该年每月的销售汇总。</li>                <li>点击【月度汇总】左侧的“+”号进行“展开”操作，可以查看该月全部订单明细。</li>                <li>点击【年/月度汇总】左侧的“—”号进行“折叠”操作，可以将明细数据进行隐藏操作。</li>            </ul>        </li>        <li>            <div class='li-header'>推荐阅读</div>            <ul>                <li><a href='http://blog.gcpowertools.com.cn/post/2013/07/04/ActiveReports-DrillDown.aspx?utm_source=Demo&utm_medium=activereports&utm_term=controlexplorer&utm_content=blog&utm_campaign=win' target='_blank'>ActiveReports 报表应用教程 (10)---交互式报表之向下钻取（详细数据按需显示解决方案）</a></li>            </ul>        </li>    </ul></div></body></html>";
            //webBrowser1.Url = new Uri("http://www.grapecity.com.cn/enterprise-solutions/activereports_server/");
        }
    }
}
