﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace GrapecityReportsLibrary.Controls
{
        public static class Colors
        {
        /// <summary>
        /// 分类默认颜色
        /// </summary>
        public static Color HeaderNormalColor = ColorTranslator.FromHtml("#5a4b7d");

        /// <summary>
        /// 分类选中颜色
        /// </summary>
        public static Color HeaderSelectedColor = ColorTranslator.FromHtml("#392e56");

        /// <summary>
        /// 报表默认颜色
        /// </summary>
        public static Color ReportNormalColor = ColorTranslator.FromHtml("#534262");

        /// <summary>
        /// 报表选中颜色
        /// </summary>
        public static Color ReportSelectColor = ColorTranslator.FromHtml("#463753");

        /// <summary>
        /// 报表鼠标悬停演示
        /// </summary>
        public static Color ReportHoverColor = ColorTranslator.FromHtml("#614d72");

        /// <summary>
        /// 按钮的鼠标悬停颜色
        /// </summary>
        public static Color ButtonHoverColor = ColorTranslator.FromHtml("#94958b");

        /// <summary>
        /// 按钮背景色
        /// </summary>
        public static Color ButtonNormalColor = ColorTranslator.FromHtml("#424242");

    }

    public class Category
    {
        /// <summary>
        /// 类别编号
        /// </summary>
        public int ID
        { get; set; }

        /// <summary>
        /// 报表分类名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 系统分类名称，行业案例或者报表功能
        /// </summary>
        public string SystemCategory { get; set; }

        /// <summary>
        /// 分类图标名称
        /// </summary>
        public string Icon { get; set; }
        
        /// <summary>
        /// 该分类包含新报表模板
        /// </summary>
        public bool HasNewReports { get; set; }

    }

    public class Report
    {
        /// <summary>
        /// 报表编号
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 报表在系统中的分类名称
        /// </summary>
        public string SystemCategory { get; set; }

        /// <summary>
        /// 报表在系统中的分类顺序
        /// </summary>
        public int SystemCategoryOrder { get; set; }

        /// <summary>
        /// 报表的功能分类名称
        /// </summary>
        public string FunctionCateory { get; set; }

        /// <summary>
        /// 报表在功能分类中的顺序
        /// </summary>
        public int FunctionCateoryOrder { get; set; }

        /// <summary>
        /// 报表名称
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// 报表存放路径
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 报表功能描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 参考资料网址
        /// </summary>
        public string ReferURL { get; set; }
        
        /// <summary>
        /// 报表当前版本号
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// 报表创建时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 最后一次更新时间
        /// </summary>
        public Nullable<DateTime> LastModifiedDate { get; set; }

        /// <summary>
        /// 最后一次更新说明
        /// </summary>
        public string ModifiedComment { get; set; }
        
        /// <summary>
        /// 系统推荐查看报表
        /// </summary>
        public bool IsSystem { get; set; }      
        
        /// <summary>
        /// 隐藏
        /// </summary>
        public bool IsHide { get; set; }

        /// <summary>
        /// Web Url
        /// </summary>
        public string WebUrl { get; set; }

        /// <summary>
        /// 是否新增
        /// </summary>
        public bool IsNew { get; set; }
    }
    
    public class MenuItem
    {
        /// <summary>
        /// 报表分类
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// 分类中的报表
        /// </summary>
        public List<Report> Reports { get; set; }
    }
}
