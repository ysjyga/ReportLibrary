﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class IndustryControl : GCRLBaseControl
    {
        public delegate void ReportClickEvent(object sender, EventArgs e);

        public event ReportClickEvent ReportClick;

        public IndustryControl()
        {
            InitializeComponent();
        }

        private void homeItemControl1_Click(object sender, EventArgs e)
        {
            if (ReportClick != null)
            {
                ReportClick(sender, e);
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

    }
}
