﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace GrapecityReportsLibrary.Controls
{
    public partial class FunctionControl : GCRLBaseControl
    {
        public delegate void ReportClickEvent(object sender, EventArgs e);

        public event ReportClickEvent ReportClick;

        public FunctionControl()
        {
            InitializeComponent();

            List<Category> list_Category = new List<Category>();
        }

        private void homeItemControl1_Click(object sender, EventArgs e)
        {

        }

        private List<Category> GetCategory()
        {
            List<Category> categorys = new List<Category>();

            string conStr = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=Data\GrapecityReportsLibrary.mdb;Persist Security Info=False";

            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();

            using (OleDbDataAdapter oda = new OleDbDataAdapter("SELECT DISTINCT 系统分类,功能分类序号,功能分类 FROM 报表功能;", conStr))
            {
                if (new OleDbConnection().State == ConnectionState.Closed)
                {
                    new OleDbConnection(conStr).Open();
                }

                oda.Fill(dt);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Category category = new Category();
                category.HasNewReports = false;
                category.Icon = dt.Rows[i]["功能分类"].ToString() + ".png";
                category.Name = dt.Rows[i]["功能分类"].ToString();
                category.SystemCategory = dt.Rows[i]["系统分类"].ToString();

                categorys.Add(category);
            }
            
            return categorys;
        }

    }
}
