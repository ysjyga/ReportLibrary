﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ReportViewerControl : GCRLBaseControl
    {
        public ReportViewerControl()
        {
            InitializeComponent();
        }

        private Report _report;
        public void LoadReport(Report report)
        {
            this._report = report;
            
            string filepath = string.Format(@"Reports\{0}\{1}\{2}", report.SystemCategory.Replace(" ", ""), report.FunctionCateory.Replace(" ", ""), report.Path);

            //if (this._report.FunctionCateory == "动态表格及数据过滤")
            //{
            //    this.viewer1.Sidebar.ThumbnailsPanel.Enabled = false;
            //    this.viewer1.Sidebar.ThumbnailsPanel.Visible = false;
            //    this.viewer1.Sidebar.SearchPanel.Enabled = false;
            //    this.viewer1.Sidebar.SearchPanel.Visible = false;
            //    this.viewer1.Sidebar.TocPanel.Enabled = false;
            //    this.viewer1.Sidebar.TocPanel.Visible = false;
            //    this.viewer1.Sidebar.ParametersPanel.Enabled = true;
            //    this.viewer1.Sidebar.ParametersPanel.Visible = true;
            //    this.viewer1.Sidebar.SelectedIndex = 3;
            //    this.viewer1.Sidebar.Visible = true;


            //}
            //else
            //    this.viewer1.Sidebar.Visible = false;

            if (this._report.Path.Contains(".rdlx"))
            {
                GrapeCity.ActiveReports.PageReport rpt = new GrapeCity.ActiveReports.PageReport(new System.IO.FileInfo(filepath));

                viewer1.LoadDocument(rpt.Document);
                viewer1.Zoom = -2;
            }
            else
            {
                GrapeCity.ActiveReports.SectionReport rptSR = new GrapeCity.ActiveReports.SectionReport();
                rptSR.LoadLayout(filepath);
                viewer1.Document = rptSR.Document;
                rptSR.Run();
            }

            this.reportInfoControl1.CurrentReport = report;

            G_Global.CallWebApi("OPEN REPORT", report.ID, report.Name, "");

        }

        private void ReportViewerControl_Load(object sender, EventArgs e)
        {
            this.viewer1.Toolbar.ToolStrip.Height = 200;
        }

        private void reportInfoControl1_ReportModified(object sender, EventArgs e)
        {
            LoadReport(this._report);
        }
    }
}
