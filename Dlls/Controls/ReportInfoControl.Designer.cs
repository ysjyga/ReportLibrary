﻿namespace GrapecityReportsLibrary.Controls
{
    partial class ReportInfoControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportInfoControl));
            this.reportDescriptionControl1 = new GrapecityReportsLibrary.Controls.ReportDescriptionControl();
            this.buttonControl3 = new GrapecityReportsLibrary.Controls.ButtonControl();
            this.panel15 = new System.Windows.Forms.Panel();
            this.buttonControl2 = new GrapecityReportsLibrary.Controls.ButtonControl();
            this.buttonControl4 = new GrapecityReportsLibrary.Controls.ButtonControl();
            this.buttonControl1 = new GrapecityReportsLibrary.Controls.ButtonControl();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel15.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // reportDescriptionControl1
            // 
            this.reportDescriptionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportDescriptionControl1.EventData = null;
            this.reportDescriptionControl1.EventName = null;
            this.reportDescriptionControl1.Location = new System.Drawing.Point(10, 158);
            this.reportDescriptionControl1.Margin = new System.Windows.Forms.Padding(0);
            this.reportDescriptionControl1.Name = "reportDescriptionControl1";
            this.reportDescriptionControl1.Padding = new System.Windows.Forms.Padding(0, 1, 0, 0);
            this.reportDescriptionControl1.Size = new System.Drawing.Size(260, 410);
            this.reportDescriptionControl1.TabIndex = 13;
            // 
            // buttonControl3
            // 
            this.buttonControl3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.buttonControl3.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(66)))), ((int)(((byte)(98)))));
            this.buttonControl3.ButtonText = "讨论该报表的功能";
            this.buttonControl3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonControl3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.buttonControl3.EventData = null;
            this.buttonControl3.EventName = null;
            this.buttonControl3.EventType = null;
            this.buttonControl3.EventValue = null;
            this.buttonControl3.Font = new System.Drawing.Font("Microsoft YaHei", 10F);
            this.buttonControl3.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonControl3.Icon = ((System.Drawing.Image)(resources.GetObject("buttonControl3.Icon")));
            this.buttonControl3.Location = new System.Drawing.Point(10, 568);
            this.buttonControl3.Margin = new System.Windows.Forms.Padding(0);
            this.buttonControl3.Name = "buttonControl3";
            this.buttonControl3.Size = new System.Drawing.Size(260, 40);
            this.buttonControl3.TabIndex = 12;
            this.buttonControl3.Visible = false;
            this.buttonControl3.ButtonClick += new GrapecityReportsLibrary.Controls.ButtonControl.ButtonClickEvent(this.buttonControl3_ButtonClick);
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(83)))), ((int)(((byte)(120)))));
            this.panel15.Controls.Add(this.buttonControl2);
            this.panel15.Controls.Add(this.buttonControl4);
            this.panel15.Controls.Add(this.buttonControl1);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(10, 0);
            this.panel15.Margin = new System.Windows.Forms.Padding(0);
            this.panel15.Name = "panel15";
            this.panel15.Padding = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.panel15.Size = new System.Drawing.Size(260, 158);
            this.panel15.TabIndex = 3;
            // 
            // buttonControl2
            // 
            this.buttonControl2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl2.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl2.ButtonText = "实现教程";
            this.buttonControl2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonControl2.EventData = null;
            this.buttonControl2.EventName = null;
            this.buttonControl2.EventType = null;
            this.buttonControl2.EventValue = null;
            this.buttonControl2.Font = new System.Drawing.Font("Microsoft YaHei", 11F);
            this.buttonControl2.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonControl2.Icon = global::GrapecityReportsLibrary.Properties.Resources.实现教程;
            this.buttonControl2.Location = new System.Drawing.Point(20, 109);
            this.buttonControl2.Margin = new System.Windows.Forms.Padding(0);
            this.buttonControl2.Name = "buttonControl2";
            this.buttonControl2.Size = new System.Drawing.Size(220, 40);
            this.buttonControl2.TabIndex = 9;
            // 
            // buttonControl4
            // 
            this.buttonControl4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl4.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl4.ButtonText = "导出报表";
            this.buttonControl4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonControl4.EventData = null;
            this.buttonControl4.EventName = null;
            this.buttonControl4.EventType = null;
            this.buttonControl4.EventValue = null;
            this.buttonControl4.Font = new System.Drawing.Font("Microsoft YaHei", 11F);
            this.buttonControl4.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonControl4.Icon = ((System.Drawing.Image)(resources.GetObject("buttonControl4.Icon")));
            this.buttonControl4.Location = new System.Drawing.Point(20, 60);
            this.buttonControl4.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.buttonControl4.Name = "buttonControl4";
            this.buttonControl4.Size = new System.Drawing.Size(220, 40);
            this.buttonControl4.TabIndex = 8;
            this.buttonControl4.ButtonClick += new GrapecityReportsLibrary.Controls.ButtonControl.ButtonClickEvent(this.buttonControl4_Click);
            // 
            // buttonControl1
            // 
            this.buttonControl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl1.ButtonBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(192)))), ((int)(((byte)(193)))));
            this.buttonControl1.ButtonText = "编辑报表";
            this.buttonControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonControl1.EventData = null;
            this.buttonControl1.EventName = null;
            this.buttonControl1.EventType = null;
            this.buttonControl1.EventValue = null;
            this.buttonControl1.Font = new System.Drawing.Font("Microsoft YaHei", 11F);
            this.buttonControl1.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonControl1.Icon = global::GrapecityReportsLibrary.Properties.Resources.修改报表;
            this.buttonControl1.Location = new System.Drawing.Point(20, 10);
            this.buttonControl1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.buttonControl1.Name = "buttonControl1";
            this.buttonControl1.Size = new System.Drawing.Size(220, 40);
            this.buttonControl1.TabIndex = 6;
            this.buttonControl1.ButtonClick += new GrapecityReportsLibrary.Controls.ButtonControl.ButtonClickEvent(this.buttonControl1_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.pictureBox1);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Margin = new System.Windows.Forms.Padding(0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(10, 608);
            this.panel12.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::GrapecityReportsLibrary.Properties.Resources.Close;
            this.pictureBox1.Location = new System.Drawing.Point(0, 279);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // ReportInfoControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.reportDescriptionControl1);
            this.Controls.Add(this.buttonControl3);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel12);
            this.Name = "ReportInfoControl";
            this.Size = new System.Drawing.Size(270, 608);
            this.panel15.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel15;
        private ButtonControl buttonControl1;
        private ButtonControl buttonControl3;
        public ReportDescriptionControl reportDescriptionControl1;
        private ButtonControl buttonControl4;
        private ButtonControl buttonControl2;
    }
}
