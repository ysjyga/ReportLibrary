﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class HomeItemControl : GCRLBaseControl
    {
        /// <summary>
        /// 当前报表显示的功能名称
        /// </summary>
        public string ReportID { get; set; }

        /// <summary>
        /// 当前报表名称
        /// </summary>
        public string FunctionName { get { return this.label1.Text; } set { this.label1.Text = value; } }

        /// <summary>
        /// 报表功能描述
        /// </summary>
        public string ReportToolTip { get; set; }

        public bool IsNewReport
        {
            get { return this.label1.Image != null; }
            set
            {
                if (value)
                {
                    this.label1.Image = Properties.Resources.red_new;
                    this.label1.ImageAlign = ContentAlignment.MiddleRight;
                }
                else
                {
                    this.label1.Image = null;
                }
            }
        }

        /// <summary>
        /// 报表显示的图片
        /// </summary>
        public Image Icon { get { return this.pictureBox1.Image; } set { this.pictureBox1.Image = value; } }

        public delegate void ReportClickEvent(object sender, EventArgs e);

        public event ReportClickEvent ReportClick;

        public HomeItemControl()
        {
            InitializeComponent();
        }

        private void HomeItemControl_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.LightGray;
        }

        private void HomeItemControl_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = Color.White;
        }

        private void HomeItemControl_Click(object sender, EventArgs e)
        {
            if (ReportClick != null)
            {
                //CommonFunctions.AddARDemoOperation(EventTypes.首页查看报表, this.ReportID);

                ReportClick(this, e);
            }
        }
    }
}
