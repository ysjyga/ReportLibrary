﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class ReportBrowserViewerControl : GCRLBaseControl
    {
        public ReportBrowserViewerControl()
        {
            InitializeComponent();
        }
        
        public void LoadReport(string url)
        {
            this.webBrowser1.Url = new Uri(url);
        }

        private void ReportViewerControl_Load(object sender, EventArgs e)
        {
            
        }

        private void reportInfoControl1_ReportModified(object sender, EventArgs e)
        {
            
        }
    }
}
