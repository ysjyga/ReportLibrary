﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrapecityReportsLibrary.Controls
{
    public partial class LinkButtonControl : GCRLBaseControl
    {
        public delegate void ButtonClickEvent(object sender, EventArgs e);

        public event ButtonClickEvent ButtonClick;

        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonText { get { return label1.Text; } set { this.label1.Text = value; } }
        /// <summary>
        /// 对应的超链接地址
        /// </summary>
        public string LinkUrl { get; set; }

        private Color _backcolor;
        /// <summary>
        /// 当前按钮的背景色
        /// </summary>
        public Color ButtonBackColor
        {
            get { return this._backcolor; }
            set { this._backcolor = value; this.BackColor = value; }
        }
        
        /// <summary>
        /// 当前按钮对应事件的类型名称
        /// </summary>
        public string EventType { get; set; }

        /// <summary>
        /// 当前按钮事件操作的对象
        /// </summary>
        public string EventValue { get; set; }

        public LinkButtonControl()
        {
            InitializeComponent();
        }

        private void ButtonControl_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.FromArgb(150, this._backcolor);
        }

        private void ButtonControl_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = this._backcolor;
        }

        private void ButtonControl_Load(object sender, EventArgs e)
        {
            this.BackColor = this._backcolor;
        }

        private void ButtonControl_Click(object sender, EventArgs e)
        {
            if (ButtonClick !=null)
            {
                ButtonClick(this, e);
            }
        }
    }
}
