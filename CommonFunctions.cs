﻿using Grapecity.PowerTools.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Windows.Forms;

namespace GrapecityReportsLibrary
{
    public static class CommonFunctions
    {
        public static bool CheckInstalledSqliteDriver()
        {
            bool installed = false;
            Microsoft.Win32.RegistryKey reg = (Microsoft.Win32.Registry.LocalMachine).OpenSubKey("Software");

            if (reg != null)
            {
                reg = reg.OpenSubKey("ODBC");
                if (reg != null)
                {
                    reg = reg.OpenSubKey("ODBCINST.INI");
                    if (reg != null)
                    {

                        reg = reg.OpenSubKey("ODBC Drivers");
                        if (reg != null)
                        {
                            foreach (var item in reg.GetValueNames())
                            {
                                if (item.ToUpper() == "SQLITE3 ODBC DRIVER".ToUpper())
                                {
                                    installed = true;
                                    break;
                                }
                            }
                        }
                        try
                        {
                            reg.Close();
                        }
                        catch { 
                            /* ignore this exception if we couldn't close */
                            return installed;
                        }
                    }
                }
            }

            return installed;
        }

        public static bool CheckIsFirstInstalled()
        {
            bool isFirst = false;
            if (!File.Exists(@"Customer\customer.ini"))
            {
                isFirst = true;
                Directory.CreateDirectory("Customer");
                File.Create(@"Customer\customer.ini");
            }

            return isFirst;
        }
    }

    public static class EventTypes
    {
        public static string 安装报表库 = "安装报表库";
        public static string 打开模板库 = "打开模板库";
        public static string 关闭模板库 = "关闭模板库";
        public static string 查看报表 = "查看报表";
        public static string 首页查看报表 = "首页查看报表";
        public static string 开始报表编辑 = "开始报表编辑";
        public static string 结束报表编辑 = "结束报表编辑";
        public static string 开始查看教程 = "开始查看教程";
        public static string 结束查看教程 = "结束查看教程";
        public static string 用户心跳检测 = "用户心跳检测";
        public static string 用户评分 = "用户评分";
        public static string 收藏报表 = "收藏报表";
        public static string 取消收藏 = "取消收藏";
        public static string 提交反馈 = "提交反馈";
        public static string 点击左上角Logo = "点击左上角Logo";
        public static string 切换报表菜单 = "切换报表菜单";
        public static string 点击菜单 = "点击菜单";
        public static string 点击广告 = "点击广告";
    }
}
