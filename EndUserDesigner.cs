using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing.Design;
using System.IO;
using GrapeCity.ActiveReports.Design;
using GrapeCity.ActiveReports.Design.Resources;
using System.ComponentModel.Design;
using GrapeCity.ActiveReports.PageReportModel;
using GrapeCity.ActiveReports;
using System.Drawing;

namespace GrapecityReportsLibrary
{
    public partial class EndUserDesigner : Form
    {
        ToolStripDropDownItem _fileMenu;
        Boolean Openflag = false;
        string _reportName;
        string _dbName;
        //DefaultViewer dfv=null;

        /// <summary>
        /// 当前编辑的报表名称
        /// </summary>
        public Controls.Report CurrentReport
        { get; set; }

        public EndUserDesigner(Controls.Report report)
        {
            InitializeComponent();

            this.CurrentReport = report;

            if (report.Path.IndexOf(".rdlx") != -1)
            {
                splitContainerMiddle.Panel2Collapsed = false;
            }
            else
            {
                splitContainerMiddle.Panel2Collapsed = true;
            }
            //Set the ToolBox, ReportExplorer and PropertyGrid in the Designer.
            reportDesigner.Toolbox = reportToolbox;//Attaches the toolbox to the report designer
            reportExplorer.ReportDesigner = reportDesigner;//Attaches the report explorer to the report designer
            reportDesigner.LayoutChanged += new LayoutChangedEventHandler(OnLayoutChanged);
            layerList.ReportDesigner = reportDesigner;
            reportDesigner.PropertyGrid = reportPropertyGrid;//Attaches the Property Grid to the report designer

            groupEditor.ReportDesigner = reportDesigner;
            //Populate the menu.
            ToolStrip toolstrip = reportDesigner.CreateToolStrips(new DesignerToolStrips[]
                {
                    DesignerToolStrips.Menu
                })[0];
            toolstrip.Items[1].Visible = false;

            _fileMenu = (ToolStripDropDownItem)toolstrip.Items[0];
            CreateFileMenu(_fileMenu);
            AppendToolStrips(0, new ToolStrip[]
                {
                    toolstrip
                });
            AppendToolStrips(1, reportDesigner.CreateToolStrips(new DesignerToolStrips[]
                {
                    DesignerToolStrips.Edit,
                    DesignerToolStrips.Undo,
                    DesignerToolStrips.Zoom
                }));
            ToolStrip item = CreateReportToolbar();
            AppendToolStrips(1, new ToolStrip[]
                {
                    item
                });
            AppendToolStrips(2, reportDesigner.CreateToolStrips(new DesignerToolStrips[]
                {
                    DesignerToolStrips.Format,
                    DesignerToolStrips.Layout
                }));
            LoadTools(reportToolbox);
            reportDesigner.LayoutChanged += (sender, args) => { if (args.Type == LayoutChangeType.ReportLoad || args.Type == LayoutChangeType.ReportClear) RefreshExportEnabled(); };
            RefreshExportEnabled();
            CreateReportExplorer();
        }
        private void OnLayoutChanged(object sender, LayoutChangedArgs e)
        {
            if (e.Type == LayoutChangeType.ReportLoad || e.Type == LayoutChangeType.ReportClear)
            {
                reportToolbox.Reorder(reportDesigner);
                reportToolbox.EnsureCategories();
                reportToolbox.Refresh();
                RefreshExportEnabled();
                CreateReportExplorer();
            }
            if (e.Type == LayoutChangeType.ReportClear)
            {
                _reportName = null;
            }
            if (e.Type == LayoutChangeType.ReportLoad)
            {
                if (!string.IsNullOrEmpty(_reportName))
                {
                    if (GetIsMaster())
                    {
                        _reportName = null;
                    }
                }
            }
        }
        private void RefreshExportEnabled()
        {
            reportDesigner.ActiveTabChanged -= OnEnableExport;
            reportDesigner.ActiveTabChanged += OnEnableExport;
            OnEnableExport(this, EventArgs.Empty);
        }
        private void OnEnableExport(object sender, EventArgs eventArgs)
        {
            //_fileMenu.DropDownItems[2].Enabled = reportDesigner.ActiveTab == DesignerTab.Preview;
        }
        private static void LoadTools(IToolboxService toolbox)
        {
            //Add Data Providers.
            foreach (Type type in new Type[]
                {
                    typeof (System.Data.DataSet),
                    typeof (System.Data.DataView),
                    typeof (System.Data.OleDb.OleDbConnection),
                    typeof (System.Data.OleDb.OleDbDataAdapter),
                    typeof (System.Data.Odbc.OdbcConnection),
                    typeof (System.Data.Odbc.OdbcDataAdapter),
                    typeof (System.Data.SqlClient.SqlConnection),
                    typeof (System.Data.SqlClient.SqlDataAdapter)
                })
            {
                toolbox.AddToolboxItem(new ToolboxItem(type), Properties.Resources.ToolBoxData);
            }
        }
        //Adding DropDownItems to the ToolStripDropDownItem.
        private void CreateFileMenu(ToolStripDropDownItem fileMenu)
        {
            fileMenu.DropDownItems.Clear();
            //         fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.NewText, Properties.Resources.CmdNewReport, new EventHandler(OnNew), (Keys.Control | Keys.N)));
            //         fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.OpenText, Properties.Resources.CmdOpen, new EventHandler(OnOpen), (Keys.Control | Keys.O)));
            fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.SaveText, Properties.Resources.CmdSave, new EventHandler(OnSave), (Keys.Control | Keys.S)));
            fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.ExportText, Properties.Resources.export, new EventHandler(OnExport), (Keys.Control | Keys.E)));
            fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.SaveAsText, Properties.Resources.CmdSaveAs, new EventHandler(OnSaveAs)));
            //fileMenu.DropDownItems.Add(new ToolStripSeparator());
            fileMenu.DropDownItems.Add(new ToolStripMenuItem(Properties.Resources.ExitText, null, new EventHandler(OnExit)));
            //fileMenu.DropDownItems[2].Enabled = false;
        }
        private ToolStrip CreateReportToolbar()
        {
            return new ToolStrip(new ToolStripButton[]
            { 
				//CreateToolStripButton(Properties.Resources.NewText,Properties.Resources.CmdNewReport,new EventHandler(OnNew),Properties.Resources.NewText),
				//CreateToolStripButton(Properties.Resources.OpenText,Properties.Resources.CmdOpen,new EventHandler(OnOpen),Properties.Resources.OpenText),
				CreateToolStripButton(Properties.Resources.SaveText,Properties.Resources.CmdSave,new EventHandler(OnSave),Properties.Resources.SaveText)
            });
        }
        private bool PerformSave()
        {
            if (string.IsNullOrEmpty(_reportName) || string.IsNullOrEmpty(Path.GetDirectoryName(_reportName)) || !File.Exists(_reportName))
            {
                return PerformSaveAs();
            }
            reportDesigner.SaveReport(new FileInfo(_reportName));
            return true;
        }
        private bool PerformSaveAs()
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.Description = "请选择模板文件存放目录";
                if(dlg.ShowDialog() == DialogResult.OK)
                {
                    string path = dlg.SelectedPath;
                    //Get User SaveAs Path
                    DirectoryInfo saveFolder = new DirectoryInfo(path);
                    if (!saveFolder.Exists)
                    {
                        saveFolder.Create();
                    }

                    //Get DataBase Path
                    FileInfo _db = new FileInfo(_dbName);
                    string dbFileName = _db.Name;
                    string newDBPath = Path.Combine(path, dbFileName);
                    _db.CopyTo(newDBPath, true);

                    //Get Report Template Path
                    FileInfo _report = new FileInfo(_reportName);
                    string reportFileName = _report.Name;
                    string newReportPath = Path.Combine(path, reportFileName);
                    _report.CopyTo(newReportPath, true);

                    var _frep = new FileInfo(newReportPath);
                    var _srep = new PageReport(_frep);
                    try
                    {
                        _srep.Report.DataSources[0].ConnectionProperties.ConnectString = "DRIVER=SQLITE3 ODBC DRIVER;DATABASE=" + newDBPath;
                        _srep.Save(_frep);
                    }
                    catch (Exception ex)
                    { }

                    bool isInstalled = JudgeARInstalled();

                    if (isInstalled)
                    {
                        MessageBox.Show("报表模板导出成功 ");
                    }
                    else
                    {
                        if (MessageBox.Show("该报表模板由 ActiveReports 报表工具制作，为更好的使用该模板交互功能，请前往官网下载最新版本的 ActiveReports ！ ", "导出报表模板完成", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                        {
                            System.Diagnostics.Process.Start("https://www.grapecity.com.cn/download?pid=16");
                        }
                    }
                    
                    return true;
                }
            }
            return false;
        }
        private bool GetIsMaster()
        {
            bool isMaster = false;
            if (reportDesigner.ReportType == DesignerReportType.Rdl)
            {
                var report = (Component)reportDesigner.Report;
                var site = report == null ? null : report.Site;
                if (site != null)
                {
                    var host = site.GetService(typeof(IDesignerHost)) as IDesignerHost;
                    if (host != null)
                    {
                        var mcs = host.GetService(typeof(IMasterContentService)) as IMasterContentService;
                        isMaster = mcs != null && mcs.IsMaster;
                    }
                }
            }
            return isMaster;
        }
        private string GetSaveFilter(DesignerReportType type, bool isMaster)
        {
            switch (type)
            {
                case DesignerReportType.Section:
                    return Properties.Resources.SectionReportFilter;
                case DesignerReportType.Page:
                    return Properties.Resources.PageReportFilter;
                case DesignerReportType.Rdl:
                    return isMaster ? Properties.Resources.RDLReportFilterMaster : Properties.Resources.RDLReportFilter;
                default:
                    return Properties.Resources.SectionReportFilter;
            }
        }
        //Click "New" to open a new report.
        private void OnNew(object sender, EventArgs e)
        {
            if (ConfirmSaveChanges())
            {
                reportDesigner.ExecuteAction(DesignerAction.NewReport);
                EnableTabs();
            }
        }
        //Click "Open" to open an existing report.
        private void OnOpen(object sender, EventArgs e)
        {
            if (!ConfirmSaveChanges())
            {
                return;
            }
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Filter = Properties.Resources.OpenFileFilter;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    _reportName = dlg.FileName;
                    reportDesigner.LoadReport(new FileInfo(dlg.FileName));
                    EnableTabs();
                }
            }
        }
        public void OnDesign(Controls.Report report)
        {
            Openflag = true;
            _reportName = report.Path;
            string filename = string.Format(@"{0}\{1}\{2}\{3}\{4}", Application.StartupPath, "Reports", report.SystemCategory, report.FunctionCateory, report.Path);
            //Get DB Path
            _dbName = string.Format(@"{0}\{1}\{2}", Application.StartupPath, "Data", "ArsDemo.db");
            _reportName = filename;
            reportDesigner.LoadReport(new FileInfo(filename));
        }
        private void OnExport(object sender, EventArgs e)
        {
            // Set the rendering extension and render the report.
            GrapeCity.ActiveReports.Export.Image.Page.ImageRenderingExtension imageRenderingExtension = new GrapeCity.ActiveReports.Export.Image.Page.ImageRenderingExtension();
            GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider outputProvider = new GrapeCity.ActiveReports.Rendering.IO.MemoryStreamProvider();
            GrapeCity.ActiveReports.Export.Image.Page.Settings setting = new GrapeCity.ActiveReports.Export.Image.Page.Settings();
            setting.ImageType = GrapeCity.ActiveReports.Export.Image.Page.Renderers.ImageType.JPEG;

            ((PageReport)reportDesigner.Report).Document.Render(imageRenderingExtension, outputProvider, setting);

            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                // Get the first page of the report
                outputProvider.GetSecondaryStreams()[0].OpenStream().CopyTo(ms);

                Bitmap bmSave = new Bitmap(System.Drawing.Image.FromStream(ms));

                bool isInstalled = JudgeARInstalled();

                if (true)
                {
                    string exportPath = "";
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Filter = "报表文件(*.jpg)|*.jpg|所有文件|*.*";
                    sfd.FileName = "报表导出文件.jpg";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        exportPath = sfd.FileName;
                        bmSave.Save(exportPath);
                        MessageBox.Show("报表文件导出成功！", "导出报表", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    if (MessageBox.Show("本程序中所有报表模板均由ActiveReports设计制作，是否去下载安装？", "检测到未安装ActiveReports", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
                    {
                        System.Diagnostics.Process.Start("https://www.grapecity.com.cn/download/?pid=16");
                    }
                }
            }
        }

        private bool JudgeARInstalled()
        {
            Microsoft.Win32.RegistryKey uninstallNode = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
            foreach (string subKeyName in uninstallNode.GetSubKeyNames())
            {
                Microsoft.Win32.RegistryKey subKey = uninstallNode.OpenSubKey(subKeyName);
                object displayName = subKey.GetValue("DisplayName");
                if (displayName != null)
                {
                    if (displayName.ToString().Contains("ActiveReports"))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        //Click "Save" to save a report.
        private void OnSave(object sender, EventArgs e)
        {
            if (Openflag == true)
            {
                reportDesigner.SaveReport(new FileInfo(_reportName));
                MessageBox.Show("你已成功修改当前报表！", "保存报表", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }


            else PerformSave();
        }
        //Click "Save as" to save a report with a name.
        private void OnSaveAs(object sender, EventArgs e)
        {
            PerformSaveAs();
        }
        private void OnExit(object sender, EventArgs e)
        {
            Close();
        }
        //Checking whether modifications have been made to the report loaded to the designer.
        private bool ConfirmSaveChanges()
        {
            if (reportDesigner.IsDirty)
            {
                DialogResult dialogresult = MessageBox.Show(Properties.Resources.ReportDirtyMessage, "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dialogresult == DialogResult.Cancel)
                {
                    return false;
                }
                if (dialogresult == DialogResult.Yes)
                {
                    return PerformSave();
                }
            }
            return true;
        }
        private void AppendToolStrips(int row, IList<ToolStrip> toolStrips)
        {
            ToolStripPanel topToolStripPanel = toolStripContainer.TopToolStripPanel;
            int num = toolStrips.Count;
            while (--num >= 0)
            {
                topToolStripPanel.Join(toolStrips[num], row);
            }
        }
        private static ToolStripButton CreateToolStripButton(string text, System.Drawing.Image image, EventHandler handler, string toolTip)
        {
            return new ToolStripButton(text, image, handler)
            {
                DisplayStyle = ToolStripItemDisplayStyle.Image,
                ToolTipText = toolTip,
                DoubleClickEnabled = true
            };
        }
        private void CreateReportExplorer()
        {
            if (reportDesigner.ReportType == DesignerReportType.Section)
            {
                if (explorerPropertyGridContainer.Panel1.Controls.Contains(reportExplorerTabControl))
                {
                    reportExplorerTabControl.TabPages[0].SuspendLayout();
                    explorerPropertyGridContainer.Panel1.SuspendLayout();
                    explorerPropertyGridContainer.Panel1.Controls.Remove(reportExplorerTabControl);
                    explorerPropertyGridContainer.Panel1.Controls.Add(reportExplorer);
                    reportExplorerTabControl.TabPages[0].ResumeLayout();
                    explorerPropertyGridContainer.Panel1.ResumeLayout();
                }
            }
            else if (!explorerPropertyGridContainer.Panel1.Controls.Contains(reportExplorerTabControl))
            {
                reportExplorerTabControl.TabPages[0].SuspendLayout();
                explorerPropertyGridContainer.Panel1.SuspendLayout();
                explorerPropertyGridContainer.Panel1.Controls.Remove(reportExplorer);
                reportExplorerTabControl.TabPages[0].Controls.Add(reportExplorer);
                explorerPropertyGridContainer.Panel1.Controls.Add(reportExplorerTabControl);
                reportExplorerTabControl.TabPages[0].ResumeLayout();
                explorerPropertyGridContainer.Panel1.ResumeLayout();
            }
        }
        private void EnableTabs()
        {
            reportToolbox.Reorder(reportDesigner);
            reportToolbox.EnsureCategories();
            reportToolbox.Refresh();
        }

        private void EndUserDesigner_Load(object sender, EventArgs e)
        {
            //CommonFunctions.AddARDemoOperation(EventTypes.开始报表编辑, this.CurrentReport.Name);
            this.OnDesign(CurrentReport);
            this.Text += string.Format("({0})", CurrentReport.Name);
        }

        private void ReportDesignerForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            //CommonFunctions.AddARDemoOperation(EventTypes.结束报表编辑, this.CurrentReport.Name);
        }
    }
}
