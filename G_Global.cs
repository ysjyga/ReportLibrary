﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GrapecityReportsLibrary
{
    public class G_Global
    {
        private static string __client_id = "";
        private static Uri __webLogAPI;
        private static string __appVersion = "20180713";


        public static void App_Init()
        {
            __client_id = FingerPrint.Value();
            __webLogAPI = new Uri("http://www.gcpowertools.com.cn/products/redirect.aspx?api=reportlibrary");
        }

        public static void CallWebApi(string operation, string memo1, string memo2, string memo3)
        {
            try
            {

                //string requestBody = "operation={0}&remote_id={1}&memo1={2}&memo2={3}&memo3={4}";
                //requestBody = string.Format(requestBody, System.Web.HttpUtility.UrlEncode(operation),
                //                                        System.Web.HttpUtility.UrlEncode(__client_id),
                //                                        System.Web.HttpUtility.UrlEncode(memo1),
                //                                        System.Web.HttpUtility.UrlEncode(memo2),
                //                                        System.Web.HttpUtility.UrlEncode(string.IsNullOrEmpty(memo3) ? __appVersion : memo3));

                //byte[] byteArray = Encoding.UTF8.GetBytes(requestBody);

                //// Post the data to the right place.
                //WebRequest request = WebRequest.Create(__webLogAPI);

                //request.Method = "POST";
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = byteArray.Length;

                //using (var dataStream = request.GetRequestStream())
                //{
                //    dataStream.Write(byteArray, 0, byteArray.Length);
                //}

                //using (var response = (HttpWebResponse)request.GetResponse())
                //{
                //    //Do what you need to do with the response.
                //}

                using (WebClient client = new WebClient())
                {
                    var values = new NameValueCollection();
                    values["operation"] = operation;
                    values["remote_id"] = __client_id;
                    values["memo1"] = memo1;
                    values["memo2"] = memo2;
                    values["memo3"] = string.IsNullOrEmpty(memo3) ? __appVersion : memo3;
#if DEBUG
                    client.UploadValuesAsync(__webLogAPI, "POST", values);
#endif
                }
            }
            catch { }
        }
    }
}
